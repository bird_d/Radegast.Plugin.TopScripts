﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows.Forms;

using Radegast;
using OpenMetaverse;

namespace Radegast.Plugin.TopScripts
{
    [Radegast.Plugin(Name = "Top Script Relay", Description = "Spouts out top scripts when issued a command.", Version = "0.1")]

    public class TopScripter : IRadegastPlugin
    {
        private RadegastInstance Instance;
        private DataTable dt;
        private System.Timers.Timer timerScripts;
        public void StartPlugin(RadegastInstance inst)
        {
            Instance = inst;
            Instance.Client.Estate.TopScriptsReply += Estate_TopScriptsReply;
            //Setup Data table
            dt = new DataTable();
            dt.Columns.Add("Time");
            dt.Columns.Add("Name");
            dt.Columns.Add("Owner");
            dt.Columns.Add("Location");
            dt.Columns.Add("Parcel");
            dt.Columns.Add("Date");
            dt.Columns.Add("URLs");
            dt.Columns.Add("Memory");

            //Setup timer
            timerScripts = new System.Timers.Timer(1000);
            timerScripts.Elapsed += TimerScripts_Elapsed;
            timerScripts.Start();
        }
        public void StopPlugin(RadegastInstance inst)
        {
            Instance.Client.Estate.TopScriptsReply -= Estate_TopScriptsReply;
            timerScripts.Elapsed -= TimerScripts_Elapsed;
            timerScripts.Stop();
            timerScripts.Dispose();
        }
        //Top Scripts Event
        private void Estate_TopScriptsReply(object sender, TopScriptsReplyEventArgs e)
        {
            dt.Clear();
            //Build Data Table
            foreach (EstateTask et in e.Tasks.Values)
            {
                DataRow row = dt.NewRow();
                row[0] = et.Score.ToString();
                row[1] = et.TaskName.ToString();
                row[2] = et.OwnerName;
                row[3] = et.Position.ToString();
                row[4] = et.TaskID.ToString();
                row[5] = et.TaskLocalID.ToString();
                row[6] = et.MonoScore.ToString();
                dt.Rows.Add(row);
            }
            //Sort
            DataRow[] sortedRows = dt.Select("", "Time DESC");
            foreach (DataRow row in sortedRows)
            {
                Instance.TabConsole.DisplayNotificationInChat(String.Join(", ", row.ItemArray));
            }
        }
        //Top Scripts Timer
        private void TimerScripts_Elapsed(object sender, ElapsedEventArgs e)
        {
            Instance.Client.Estate.RequestTopScripts();
        }
    }
}
